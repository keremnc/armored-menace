package tank.game.entity;

import tank.game.Game;
import tank.game.map.Tile;
import tank.game.player.Orientation;
import tank.game.render.Texture;

public class TileEntity extends Entity {
	private Tile tile;

	public TileEntity(int x, int y, Tile tile) {
		super(x, y);
		this.tile = tile;
	}

	@Override
	public void tick() {
		// so that it can face the proper direction as presented in the sprite
		// sheet
		orientation = Orientation.EAST;
	}

	@Override
	public Texture getTexture() {
		// returns appropriate texture based on Tile enum object
		return Game.getInstance().getSpriteSheet().getSprite(tile.getTile());
	}

	@Override
	public boolean collision(Entity other) {
		return false;
	}

	@Override
	public boolean isSolid() {
		// so that players cannot pass through obstacles
		return true;
	}
}
