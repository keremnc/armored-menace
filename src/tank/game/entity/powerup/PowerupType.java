package tank.game.entity.powerup;

import tank.game.entity.Tank;

public enum PowerupType {
	HEALTH(52, true) {
		@Override
		public void applyInsta(Tank t) {
			t.health = 100;
		}
	},
	ARMOR(54, true) {
		@Override
		public void applyInsta(Tank t) {
			t.armor = 100;
		}
	},
	SPEED(53, false),
	EXPLOSIVITY(51, false);

	private int text;
	private boolean insta;

	private PowerupType(int text, boolean insta) {
		this.text = text;
		this.insta = insta;
	}

	public int getTexture() {
		return text;
	}

	public boolean isInsta() {
		return insta;
	}

	public String fancyName() {
		return name().substring(0, 1).toUpperCase() + name().substring(1).toLowerCase();
	}

	public void applyInsta(Tank t) {}
}
