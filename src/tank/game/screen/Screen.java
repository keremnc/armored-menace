package tank.game.screen;

import java.awt.BasicStroke;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.InputEvent;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

import tank.game.Game;
import tank.game.entity.Entity;
import tank.game.entity.Tank;
import tank.game.entity.projectile.WeaponType;
import tank.game.map.GameHandler;
import tank.game.player.Orientation;
import tank.game.render.Texture;

/**
 * Class that manages all rendering and rasterization of sprites onto the
 * screen.
 *
 */
public class Screen extends Canvas {
	private static final long serialVersionUID = 1L; // because i hate warnings

	public static final int WIDTH = (int) (Toolkit.getDefaultToolkit().getScreenSize().getWidth() / 4) * 3;
	public static final int HEIGHT = (WIDTH / 16 * 9);

	public static final int HEALTH_BAR_WIDTH = WIDTH / 6;
	public static final int HEALTH_BAR_HEIGHT = HEALTH_BAR_WIDTH / 5;

	public static final int SCALE = 1;

	// image that we draw onto the screen every tick
	private final BufferedImage screen = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_ARGB);

	// raw pixels for 'screen' object
	private final int[] pixels = ((DataBufferInt) screen.getRaster().getDataBuffer()).getData();

	private JFrame frame;

	/**
	 * Initialize all canvas code, and setup some JFrame stuff
	 */
	public Screen() {

		createBufferStrategy(1);

		setMinimumSize(new Dimension(WIDTH * SCALE, HEIGHT * SCALE));
		setMaximumSize(new Dimension(WIDTH * SCALE, HEIGHT * SCALE));
		setPreferredSize(new Dimension(WIDTH * SCALE, HEIGHT * SCALE));
		setSize(WIDTH * SCALE, HEIGHT * SCALE);

		frame = new JFrame(Game.NAME);
		frame.setSize(WIDTH * SCALE, HEIGHT * SCALE);
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.add(this);
		frame.setResizable(false);
		frame.setVisible(true);
		frame.setLocationRelativeTo(null);
		frame.createBufferStrategy(1);

		setIgnoreRepaint(true);
		frame.setIgnoreRepaint(true);
		frame.toFront();
		frame.requestFocus();
		frame.setAlwaysOnTop(true);

		try {
			// remember the last location of mouse
			final Point oldMouseLocation = MouseInfo.getPointerInfo().getLocation();

			// simulate a mouse click on title bar of window
			Robot robot = new Robot();

			Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
			robot.mouseMove((int) (d.getWidth() / 2), (int) (d.getHeight() / 2));
			robot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
			robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);

			// move mouse to old location
			robot.mouseMove((int) oldMouseLocation.getX(), (int) oldMouseLocation.getY());
		}
		catch (Exception ex) {
			// just ignore exception, or you can handle it as you want
		}
		finally {
			frame.setAlwaysOnTop(false);

		}

	}

	public void hide() {
		frame.setVisible(false);
	}

	/**
	 * Method called every tick to render the screen. We use a method of
	 * rendering that consists of rendering all components onto a BufferedImage
	 * object via pixel manipulation and drawing that BufferedImage object onto
	 * the Canvas.
	 */
	public void render() {
		BufferStrategy bs = getBufferStrategy();
		if (bs == null) {
			createBufferStrategy(3);
			return;
		}

		for (int i = 0; i < pixels.length; i++) {
			pixels[i] = new Color(220, 214, 181).getRGB();
		}

		// renderBackground();

		ArrayList<Entity> oldEntities = new ArrayList<Entity>();
		oldEntities.addAll(Game.getInstance().getGameHandler().getEntities());

		for (Entity e : oldEntities) {
			if (e != null) {
				e.render();
			}
		}

		if (GameHandler.debug) {
			Graphics2D g = (Graphics2D) screen.getGraphics();

			Tank player1Tank = Game.getInstance().getGameHandler().getPlayer1Tank();
			Tank player2Tank = Game.getInstance().getGameHandler().getPlayer2Tank();

			g.setColor(Color.black);
			g.setFont(new Font("Arial", Font.BOLD, 15));
			g.drawString("Armored Menace (" + Game.totalFps + " fps, " + Game.updates + " updates)", 10, 15);
			g.drawString("rendered entities: " + Game.getInstance().getGameHandler().getEntities().size(), 10, 35);
			g.drawString("player 1 health: " + player1Tank.health, 10, 50);
			g.drawString("player 2 health: " + player2Tank.health, 10, 65);

			g.drawString("Player 1 X: " + player1Tank.x, 10, 100);
			g.drawString("Player 1 Y: " + player1Tank.y, 10, 115);

			g.drawString("Player 2 X: " + player2Tank.x, 10, 150);
			g.drawString("Player 2 Y: " + player2Tank.y, 10, 165);
			g.dispose();
		}

		drawGUI();

		Graphics gfx = bs.getDrawGraphics();
		gfx.drawImage(screen, 0, 0, WIDTH * SCALE, HEIGHT * SCALE, null);
		gfx.dispose();
		bs.show();
	}

	/**
	 * Dras the boxes on the bottom of the screen with each of the tanks and
	 * their data, and the health/armor bars.
	 */
	public void drawGUI() {
		int x1 = (int) (WIDTH / 3), y1 = HEIGHT - 120;
		int x2 = WIDTH - x1, y2 = HEIGHT - 50;

		int boxes = 2;

		int boxWidth = (x2 - x1 - ((boxes * 4) + 1)) / boxes;

		Graphics2D g = (Graphics2D) screen.getGraphics();

		g.setStroke(new BasicStroke(4, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_ROUND));
		g.setColor(new Color(183, 183, 183));
		g.drawRect(x1, y1, x2 - x1, y2 - y1);

		g.setColor(Color.black);
		g.setStroke(new BasicStroke(2));
		g.drawRect(x1 - 2, y1 - 2, (x2 - x1) + 3, (y2 - y1) + 3);

		int addition = 2;
		for (int i = 0; i < boxes; i++) {
			int indBoxX = (boxWidth * i) + x1 + addition;
			g.setFont(new Font("Arial", Font.PLAIN, 12));

			g.setStroke(new BasicStroke(2));
			g.setColor(Color.DARK_GRAY);

			Tank t = null;
			if (i == 0) {
				t = Game.getInstance().getGameHandler().getPlayer1Tank();
			} else {
				t = Game.getInstance().getGameHandler().getPlayer2Tank();
			}

			int xMid = indBoxX + (boxWidth / 2);
			int yMid = HEIGHT - (120);

			int barDiff = 25;

			g.setColor(Color.BLUE);
			g.drawString("Player " + (i + 1), indBoxX + 10, HEIGHT - 100);

			int xStart = 0;
			int yStart = HEIGHT - HEALTH_BAR_HEIGHT - 40;

			if (i == 0) {
				g.drawString("Move: Arrow Keys", indBoxX + 10, HEIGHT - 85);
				g.drawString("Shoot: M", indBoxX + 10, HEIGHT - 70);
				xStart = barDiff;
			} else {

				g.drawString("Move: WASD Keys", indBoxX + 10, HEIGHT - 85);
				g.drawString("Shoot: E", indBoxX + 10, HEIGHT - 70);
				xStart = WIDTH - HEALTH_BAR_WIDTH - barDiff;

			}

			for (int z = 0; z < 2; z++) {

				if (z == 0) {
					g.setColor(Color.DARK_GRAY);

					g.drawRect(xStart, yStart, HEALTH_BAR_WIDTH, HEALTH_BAR_HEIGHT);
					g.setColor(Color.green);

					g.fillRect(xStart, yStart, (int) (HEALTH_BAR_WIDTH * (t.health / 100D)), HEALTH_BAR_HEIGHT);

				} else {
					g.setColor(Color.DARK_GRAY);
					g.drawRect(xStart, yStart - HEALTH_BAR_HEIGHT - 30, HEALTH_BAR_WIDTH, HEALTH_BAR_HEIGHT);
					g.setColor(Color.green);

					g.fillRect(xStart, yStart - HEALTH_BAR_HEIGHT - 30, (int) (HEALTH_BAR_WIDTH * (t.armor / 100D)), HEALTH_BAR_HEIGHT);
				}

				g.setFont(new Font("Arial", Font.BOLD, 25));
				g.setColor(Color.RED);

				if (z == 0) {
					g.drawString("HP", xStart + 20 + (HEALTH_BAR_WIDTH / 3), yStart + (HEALTH_BAR_HEIGHT / 2) + 10);
				} else {
					g.drawString("Armor", xStart + (HEALTH_BAR_WIDTH / 3), yStart + (HEALTH_BAR_HEIGHT / 2) - 20 - HEALTH_BAR_HEIGHT);

				}

			}
			g.setColor(Color.GRAY);

			g.setFont(new Font("Arial", Font.PLAIN, 12));

			g.drawImage(t.getDefaultTexture().getImage(), xMid + 20, yMid, null);
			g.drawRect(indBoxX, y1 + 3, boxWidth, (y2 - y1) - 6);

			addition += 4;

			if (t.news != null) {

				String n = t.news;

				g.setColor(Color.RED);
				g.setFont(new Font("Arial", Font.PLAIN, 13));
				g.drawString(n, t.x - 5, t.y);

				if (t.newsTill < System.currentTimeMillis()) {
					t.news = null;
				}
			}
		}

		if (Game.getInstance().getGameHandler().winner != null) {
			String winner = "Player " + Game.getInstance().getGameHandler().winner;

			g.setColor(Color.PINK);
			g.setFont(new Font("Arial", Font.BOLD, 125));
			g.drawString(winner + " wins!", 50, HEIGHT / 2);
		}

		g.dispose();
	}

	/**
	 * Generic rendering method. Draws the texture with the given orientation at
	 * the specified X and Y screen coordinates, providing an optional bounding
	 * box to draw.
	 * 
	 * @param text
	 *            the texture to draw
	 * @param or
	 *            the orientation of the texture
	 * @param x
	 *            screen X coordinate
	 * @param y
	 *            screen Y coordinate
	 * @param r
	 *            bounding box of texture
	 */
	public void render(Texture text, Orientation or, int x, int y, Rectangle r) {

		BufferedImage bi = text.getImage();

		BufferedImage lol;

		if (or != null) {
			AffineTransform at = new AffineTransform();

			at.rotate(Math.toRadians(or.getAngle()), bi.getWidth() / 2, bi.getHeight() / 2);

			lol = new AffineTransformOp(at, AffineTransformOp.TYPE_BILINEAR).filter(bi, null);

		} else {
			lol = bi;
		}

		int xMax = x + lol.getWidth();
		int yMax = y + lol.getHeight();

		for (int i = x; i < xMax; i++) {
			for (int j = y; j < yMax; j++) {
				int canvasIndex = i + (j * WIDTH);
				int rgb = lol.getRGB(i - x, j - y);

				if (canvasIndex >= pixels.length) {
					continue;
				}
				if (new Color(rgb, true).getAlpha() != 0) {
					pixels[canvasIndex] = rgb;
				}
			}
		}

		if (GameHandler.debug && r != null) {
			Graphics2D g2d = (Graphics2D) screen.getGraphics();
			int cx = text.getCenterX(x);
			int cy = text.getCenterY(y);

			g2d.drawRect(r.x, r.y, r.width, r.height);
			g2d.drawLine(cx - (r.width / 2), cy, cx + (r.width / 2), cy);
			g2d.drawLine(cx, cy + (r.height / 2), cx, cy - (r.height / 2));
		}
	}
}
