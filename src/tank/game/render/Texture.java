package tank.game.render;

import java.awt.image.BufferedImage;

public class Texture {

	private BufferedImage image;
	private int width;
	private int height;

	public Texture(BufferedImage image, int width, int height) {
		this.image = image;
		this.width = width;
		this.height = height;
	}

	public BufferedImage getImage() {
		return image;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public int getCenterX(int x) {
		return (x + x + 63) / 2;

	}

	public int getCenterY(int y) {

		return (y + y + 63) / 2;
	}
}
