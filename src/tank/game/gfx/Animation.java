package tank.game.gfx;

import java.awt.Graphics;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;

import tank.game.entity.Entity;
import tank.game.map.GameHandler;
import tank.game.render.Texture;
import tank.game.screen.Screen;

public class Animation extends Entity {
	private Texture[] textures;
	private long millisInBetween;
	private Entity attached;

	public Animation(int x, int y, Texture[] textures, long millisInBetween, Entity attached) {
		super(x, y);
		this.textures = textures;
		this.millisInBetween = millisInBetween;
		this.attached = attached;
	}

	public void render(Graphics g) {
		BufferedImage bi = getTexture().getImage();

		AffineTransform at = new AffineTransform();

		at.rotate(Math.toRadians(orientation.getAngle()), bi.getWidth() / 2, bi.getHeight() / 2);

		g.drawImage(new AffineTransformOp(at, AffineTransformOp.TYPE_BILINEAR).filter(bi, null), x, y, 64 * Screen.SCALE, 64 * Screen.SCALE, null);
	}

	@Override
	public void tick() {
		if (attached != null) {
			x = attached.x;
			y = attached.y;
		}
	}

	@Override
	public Texture getTexture() {

		long millisPast = System.currentTimeMillis() - spawnTime;

		int times = (int) (millisPast / millisInBetween);

		if (times >= textures.length) {

			destroy();
			return textures[textures.length - 1];
		}

		return textures[times];

	}

	@Override
	public boolean isSolid() {
		return false;
	}

	@Override
	public boolean collision(Entity other) {
		return false;
	}

}
