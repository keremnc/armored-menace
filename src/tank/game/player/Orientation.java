package tank.game.player;

import java.awt.event.KeyEvent;

public enum Orientation {

	NORTH(270, KeyEvent.VK_UP, KeyEvent.VK_W, 0, -1),

	EAST(0, KeyEvent.VK_RIGHT, KeyEvent.VK_D, 1, 0),

	SOUTH(90, KeyEvent.VK_DOWN, KeyEvent.VK_S, 0, 1),

	WEST(180, KeyEvent.VK_LEFT, KeyEvent.VK_A, -1, 0);

	int angle;
	int keyFacing;
	int p2keyEvent;
	int x, y;

	private Orientation(int angle, int keyEvent, int p2keyEvent, int x, int y) {
		this.angle = angle;
		this.keyFacing = keyEvent;
		this.p2keyEvent = p2keyEvent;
		this.x = x;
		this.y = y;
	}

	public int getAngle() {
		return angle;
	}

	public int getP2keyEvent() {
		return p2keyEvent;
	}

	public int getKeyFacing() {
		return keyFacing;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public Orientation getOpposite() {
		for (Orientation o : values()) {
			if (o.x == x) {
				if (o.y != y) {
					return o;
				}
			} else if (o.y == y) {
				if (o.x != x) {
					return o;
				}
			}
		}
		return null;
	}
}
