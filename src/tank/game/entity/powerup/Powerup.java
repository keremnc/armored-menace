package tank.game.entity.powerup;

import tank.game.Game;
import tank.game.entity.Entity;
import tank.game.entity.Tank;
import tank.game.player.Orientation;
import tank.game.render.Texture;

public class Powerup extends Entity {
	public PowerupType pType;
	public int duration;

	public Powerup(int x, int y, PowerupType pType, int dur) {
		super(x, y);
		orientation = Orientation.EAST;
		this.pType = pType;
		this.duration = dur;

		if (pType.isInsta()) {
			duration = 0;
		}

	}

	@Override
	public boolean isSolid() {
		return false;
	}

	@Override
	public void tick() {}

	@Override
	public Texture getTexture() {
		return Game.getInstance().getSpriteSheet().getSprite(pType.getTexture());
	}

	@Override
	public boolean collision(Entity other) {
		if (other instanceof Tank) {
			((Tank) other).addPowerup(this);
			Game.getInstance().getGameHandler().removeEntity(this);

			return true;
		}
		return false;
	}
}
