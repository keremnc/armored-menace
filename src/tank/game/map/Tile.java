package tank.game.map;

import tank.game.gfx.SpriteSheet;

public enum Tile {
	CACTUS(SpriteSheet.CACTUS),
	ROCK(SpriteSheet.ROCK),
	TREE(SpriteSheet.TREE);

	private int tile;

	private Tile(int tile) {
		this.tile = tile;
	}

	public int getTile() {
		return tile;
	}
}
