package tank.game.entity;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;

import tank.game.Game;
import tank.game.entity.powerup.Powerup;
import tank.game.entity.powerup.PowerupType;
import tank.game.entity.projectile.Missile;
import tank.game.entity.projectile.WeaponType;
import tank.game.gfx.Animator;
import tank.game.gfx.SpriteSheet;
import tank.game.map.GameHandler;
import tank.game.player.Orientation;
import tank.game.render.Texture;

public class Tank extends MovingEntity implements KeyListener {
	public static final int TANK_SPEED = 1;

	boolean hurt;
	long hurtTime;
	public int health = 100;
	public int armor = 100;

	public String news = null; // string to flash by player tank
	public long newsTill; // timestamp to show the newsflash to

	private int selectedSlot = 0;
	private HashMap<WeaponType, Long> lastShoot = new HashMap<WeaponType, Long>();
	PlayerType type;

	private HashMap<PowerupType, Long> powerups = new HashMap<PowerupType, Long>();

	public Tank(int x, int y, PlayerType type) {
		super(x, y);
		this.type = type;
	}

	@Override
	public Texture getTexture() {
		if (hurt) { // flash red and white if
			long millisAgo = System.currentTimeMillis() - hurtTime;

			if (millisAgo > 1000) {
				hurt = false;
			} else {
				int prim = (int) (millisAgo / 100);
				if (prim % 2 == 1) {
					return Game.getInstance().getSpriteSheet().getSprite(SpriteSheet.TANK_FLASH_RED);
				} else {
					return Game.getInstance().getSpriteSheet().getSprite(SpriteSheet.TANK_FLASH_WHITE);
				}

			}

		}
		if (type == PlayerType.ONE) {
			return Game.getInstance().getSpriteSheet().getSprite(SpriteSheet.TANK_NORMAL);
		} else {
			return Game.getInstance().getSpriteSheet().getSprite(SpriteSheet.ENEMY_TANK);

		}
	}

	public Texture getDefaultTexture() {
		if (type == PlayerType.ONE) {
			return Game.getInstance().getSpriteSheet().getSprite(SpriteSheet.TANK_NORMAL);
		} else {
			return Game.getInstance().getSpriteSheet().getSprite(SpriteSheet.ENEMY_TANK);

		}
	}

	public int getSelectedSlot() {
		return selectedSlot;
	}

	public WeaponType getSelectedWeapon() {
		if (selectedSlot >= WeaponType.values().length) {
			return null;
		}
		return WeaponType.values()[selectedSlot];
	}

	public void keyTyped(KeyEvent e) {}

	public void keyPressed(KeyEvent e) {
		if (destroy || dead) {
			return;
		}
		for (Orientation ot : Orientation.values()) {
			if (type == PlayerType.ONE) {
				if (ot.getKeyFacing() == e.getKeyCode()) {
					orientation = ot;
					moving = true;
				}
			} else {

				if (ot.getP2keyEvent() == e.getKeyCode()) {
					orientation = ot;
					moving = true;
				}

			}
		}

		if (e.getKeyCode() == KeyEvent.VK_E && type == PlayerType.TWO) {

			launchMissile();

		}
		if (e.getKeyCode() == KeyEvent.VK_M && type == PlayerType.ONE) {

			launchMissile();

		}
		if (e.getKeyCode() == KeyEvent.VK_BACK_QUOTE && type == PlayerType.ONE) {
			GameHandler.debug = !GameHandler.debug;
		}
	}

	public void keyReleased(KeyEvent e) {

		for (Orientation ot : Orientation.values()) {
			if (type == PlayerType.ONE) {

				if (ot.getKeyFacing() == e.getKeyCode() && ot == orientation) {
					moving = false;
				}
			} else {

				if (ot.getP2keyEvent() == e.getKeyCode() && ot == orientation) {
					moving = false;
				}

			}
		}

	}

	@Override
	public int getSpeed() {

		return TANK_SPEED * (hasPowerup(PowerupType.SPEED) ? 2 : 1);
	}

	public void launchMissile() {
		if (selectedSlot >= WeaponType.values().length) {
			return;
		}
		WeaponType wType = WeaponType.values()[selectedSlot];

		if (lastShoot.containsKey(wType) && lastShoot.get(wType) > System.currentTimeMillis()) {
			return;

		}
		lastShoot.put(wType, System.currentTimeMillis() + wType.getCooldown());

		int add = getTexture().getWidth() / 2;

		int addX = x + (orientation.getX() * add);
		int addY = y + (orientation.getY() * add);

		// adds the missile past the tanks cannon

		// if missile has a sound, play it
		String bip = wType.getSound();
		try {

			InputStream iu = Game.class.getResourceAsStream("/sounds/" + bip);
			BufferedInputStream bis = new BufferedInputStream(iu);
			if (iu != null) {
				AudioInputStream audioIn = AudioSystem.getAudioInputStream(bis);

				AudioFormat format = audioIn.getFormat();
				DataLine.Info info = new DataLine.Info(Clip.class, format);
				Clip clip = (Clip) AudioSystem.getLine(info);

				clip.open(audioIn);
				clip.start();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		Missile m = new Missile(addX, addY, this, wType);
		m.orientation = orientation;
		m.spawnIn();

	}

	/**
	 * Plays a large explosion at the location of this tank
	 */
	public void explodeBig() {

		SpriteSheet ss = Game.getInstance().getSpriteSheet();
		Texture[] t = { ss.getSprite(SpriteSheet.EXPLOSION_1),
				ss.getSprite(SpriteSheet.EXPLOSION_2),
				ss.getSprite(SpriteSheet.EXPLOSION_3),
				ss.getSprite(SpriteSheet.EXPLOSION_4),
				ss.getSprite(SpriteSheet.EXPLOSION_5),
				ss.getSprite(SpriteSheet.EXPLOSION_6),
				ss.getSprite(SpriteSheet.EXPLOSION_7) };

		Animator.animate(x, y, t, 70, this);

	}

	public void explodeSmall() {
		SpriteSheet ss = Game.getInstance().getSpriteSheet();
		Texture[] t = { ss.getSprite(SpriteSheet.EXPLOSION_1),
				ss.getSprite(SpriteSheet.EXPLOSION_2),
				ss.getSprite(SpriteSheet.EXPLOSION_3) };

		Animator.animate(x, y, t, 90, this);
	}

	public void addPowerup(Powerup p) {
		news = "Gained " + p.pType.fancyName() + (p.pType.isInsta() ? "!" : " - " + p.duration + " seconds!");

		if (p.pType.isInsta()) {
			p.pType.applyInsta(this);
		}

		newsTill = System.currentTimeMillis() + 2000;
		powerups.put(p.pType, System.currentTimeMillis() + (p.duration * 1000));
	}

	public boolean hasPowerup(PowerupType p) {
		return powerups.containsKey(p) && powerups.get(p) > System.currentTimeMillis();
	}

	public void kill() {
		Game.getInstance().getGameHandler().removeEntity(this);
		dead = true;

		final PlayerType winner;
		if (type == PlayerType.ONE) {
			winner = (PlayerType.TWO);
		} else {
			winner = (PlayerType.ONE);

		}

		Game.getInstance().getGameHandler().winner = winner.name().toLowerCase();

		TimerTask t = new TimerTask() {

			@Override
			public void run() {
				Game.getInstance().finishGame(winner);
			}

		};
		new Timer().schedule(t, 5000);
	}

	public void hurt(Missile m) {
		double dmt = m.getDamage();

		hurt = true;
		hurtTime = System.currentTimeMillis();
		explodeSmall();

		if (armor > 0) {

			armor -= dmt;

			if (armor >= 0) {
				return;
			}

			armor = 0;
			dmt = -armor;

		}
		health -= m.getDamage();

		if (health <= 0) {
			health = 0;
			kill();
			explodeBig();
			return;
		}

	}
}