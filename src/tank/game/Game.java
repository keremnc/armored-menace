package tank.game;

import java.awt.Color;

import javax.swing.JFrame;

import tank.game.entity.PlayerType;
import tank.game.gfx.ImageHandler;
import tank.game.gfx.SpriteSheet;
import tank.game.map.GameHandler;
import tank.game.menu.AboutMenu;
import tank.game.menu.MainMenu;
import tank.game.screen.Screen;

public class Game {

	public static final String NAME = "Armored Menace";
	public static final Color BG_COLOR = new Color(100, 50, 60);

	private JFrame frame;

	public boolean running;
	public int tickCount = 0;

	private SpriteSheet spriteSheet;
	private GameState gameState = GameState.MENU;
	private ImageHandler imageHandler;
	private GameHandler gameHandler;
	private Screen screen;

	public static Game gameInstance;

	/*
	 * --------- TICK LOOP MANAGEMENT ----------
	 */

	public static final double GAME_HERTZ = 120.0;
	// Calculate how many ns each frame should take for our target game
	// hertz.
	public static final double TIME_BETWEEN_UPDATES = 1000000000 / GAME_HERTZ;
	// At the very most we will update the game this many times before a new
	// render.
	// If you're worried about visual hitches more than perfect timing, set
	// this to 1.
	public static final int MAX_UPDATES_BEFORE_RENDER = 1;
	// We will need the last update time.
	public static double lastUpdateTime = System.nanoTime();
	// Store the last time we rendered.
	public static double lastRenderTime = System.nanoTime();

	// If we are able to get as high as this FPS, don't render again.
	public static final double TARGET_FPS = 60;
	public static final double TARGET_TIME_BETWEEN_RENDERS = 1000000000 / TARGET_FPS;

	// Simple way of finding FPS.
	public static int lastSecondTime = (int) (lastUpdateTime / 1000000000);

	public Game() {
		gameInstance = this;

		spriteSheet = new SpriteSheet("/images/sprite_sheet.png");
		imageHandler = new ImageHandler(spriteSheet);

		setupMenu();

	}

	public static int threadFrames = 0;
	public static int totalFps = 0;
	public static long lastUpdate = System.currentTimeMillis();

	public void runGameLoop() {
		Thread loop = new Thread() {
			public void run() {
				gameLoop();
			}
		};

		Thread rend = new Thread() {
			public void run() {
				while (gameState == GameState.RUNNING) {
					screen.render();
					threadFrames++;

					// if one second has surpassed, set FPS equal to frames that
					// were played during that second
					if (System.currentTimeMillis() >= lastUpdate + 1000) {
						lastUpdate = System.currentTimeMillis();
						totalFps = threadFrames;
						threadFrames = 0;
					}

				}
				stop();
			};
		};
		lastUpdate = System.currentTimeMillis();
		rend.start();
		loop.start();
	}

	public static int updates = 0;

	// Only run this in another Thread!
	private void gameLoop() {

		// This value would probably be stored elsewhere.

		while (true) {
			double now = System.nanoTime();
			int updateCount = 0;

			if (gameState == GameState.RUNNING) {

				// Do as many game updates as we need to, potentially playing
				// catchup.
				while (now - lastUpdateTime > TIME_BETWEEN_UPDATES && updateCount < MAX_UPDATES_BEFORE_RENDER) {
					gameHandler.tick();
					lastUpdateTime += TIME_BETWEEN_UPDATES;
					updateCount++;
				}

				// If for some reason an update takes forever, we don't want to
				// do an insane number of catchups.
				// If you were doing some sort of game that needed to keep EXACT
				// time, you would get rid of this.
				if (now - lastUpdateTime > TIME_BETWEEN_UPDATES) {
					lastUpdateTime = now - TIME_BETWEEN_UPDATES;
				}

				lastRenderTime = now;

				// Update the frames we got.
				int thisSecond = (int) (lastUpdateTime / 1000000000);
				if (thisSecond > lastSecondTime) {
					updates = updateCount;

					updateCount = 0;
					lastSecondTime = thisSecond;
				}

				// Yield until it has been at least the target time between
				// renders. This saves the CPU from hogging.
				while (now - lastRenderTime < TARGET_TIME_BETWEEN_RENDERS && now - lastUpdateTime < TIME_BETWEEN_UPDATES) {
					Thread.yield();

					try {
						Thread.sleep(1);
					}
					catch (Exception e) {}

					now = System.nanoTime();
				}
			}
		}
	}

	/*
	 * ------------- INSTANCE GETTERS --------------
	 */

	public static Game getInstance() {
		return gameInstance;
	}

	public GameState getGameState() {
		return gameState;
	}

	public GameHandler getGameHandler() {
		return gameHandler;
	}

	public SpriteSheet getSpriteSheet() {
		return spriteSheet;
	}

	public ImageHandler getImageHandler() {
		return imageHandler;
	}

	public Screen getScreen() {
		return screen;
	}

	/*
	 * ------------- GAME MASTER CONTROL --------------
	 */

	/**
	 * Clears data and opens the menu again
	 */
	public void finishGame(PlayerType winner) {

		frame.setVisible(true);
		gameState = GameState.MENU;
		screen.setVisible(false);
		screen.hide();
		screen = null;
		gameHandler = null;
	}

	/**
	 * Shows the about screen
	 */
	public void showAbout() {
		frame.dispose();
		frame.setVisible(false);

		frame = new AboutMenu();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setUndecorated(true);
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		frame.setResizable(false);

	}

	/**
	 * Sets up the main menu
	 */
	public void setupMenu() {

		if (frame != null) {
			frame.dispose();
			frame.setVisible(false);
		}

		frame = new MainMenu();
		frame.setBackground(Game.BG_COLOR);
		frame.setName(NAME);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setUndecorated(true);
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		frame.setResizable(false);
		frame.getContentPane().setBackground(Game.BG_COLOR);
		frame.getGraphics().drawImage(Game.getInstance().getImageHandler().getLogo(), 0, 0, Game.BG_COLOR, frame);

	}

	public void startGame() {
		if (gameState == GameState.RUNNING) {} else if (gameState == GameState.MENU) {
			frame.dispose();
			frame.setVisible(false);
			screen = new Screen();
			gameHandler = new GameHandler();
			gameHandler.init();
			gameState = GameState.RUNNING;
			runGameLoop();

		}
	}

	public synchronized void start() {
		running = true;
	}

	public synchronized void stop() {
		running = false;
		System.exit(0);

	}

	public static void main(String[] args) {
		new Game().start();
	}

}
