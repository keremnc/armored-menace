package tank.game.gfx;

import tank.game.entity.Entity;
import tank.game.render.Texture;

public class Animator {

	/**
	 * Plays an animation by rendering a series of textures in sequence with a
	 * common interval in between.
	 */
	public static void animate(int x, int y, Texture[] textures, long millisInBetween, Entity attached) {
		Animation a = new Animation(x, y, textures, millisInBetween, attached);
		a.spawnIn();

	}
}
