package tank.game;

public enum GameState {

	RUNNING,

	PAUSED,

	MENU
}
