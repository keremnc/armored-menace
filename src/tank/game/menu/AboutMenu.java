package tank.game.menu;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;

import tank.game.Game;

public class AboutMenu extends JFrame implements ActionListener {

	public static final int WIDTH = 200;
	public static final int HEIGHT = (WIDTH / 10 * 4);
	public static final int SCALE = 3;

	private Button menu;
	private JPanel iconPanel = new JPanel();

	public AboutMenu() {

		setMinimumSize(new Dimension(WIDTH * SCALE, HEIGHT * SCALE));
		setMaximumSize(new Dimension(WIDTH * SCALE, HEIGHT * SCALE));
		setPreferredSize(new Dimension(WIDTH * SCALE, HEIGHT * SCALE));
		setupButtons();
	}

	private void setupButtons() {
		setLocationRelativeTo(null);

		menu = new Button("Back to Menu");
		menu.setPreferredSize(new Dimension(120, 40));

		iconPanel.setLayout(new BoxLayout(iconPanel, BoxLayout.Y_AXIS));

		iconPanel.add(menu);

		JPanel mini = new JPanel();

		mini.add(iconPanel, BorderLayout.NORTH);

		add(mini, BorderLayout.SOUTH);

		menu.addActionListener(this);

		mini.setVisible(true);

		mini.setBackground(Game.BG_COLOR);
		iconPanel.setBackground(Game.BG_COLOR);
		setBackground(Game.BG_COLOR);
		getContentPane().setBackground(Game.BG_COLOR);

	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);

		Font f = new Font("Arial", Font.BOLD, 20);

		g.setColor(Color.CYAN);
		g.setFont(f);
		g.drawString("Armored Menace is a game in which two players control", 30, 60);
		g.drawString("tanks and try to eliminate the other!", 30, 80);

		f = new Font("Arial", Font.BOLD, 15);

		g.setColor(Color.green);
		g.setFont(f);

		g.drawString("Player 1 controls their tank using the arrow keys, and fires with the M key.", 30, 110);
		g.drawString("Player 2 controls their tank using the WASD keys, and fires with the E key.", 30, 130);

		g.drawString("The first player to destroy the opponent tank wins!", 30, 160);
	}

	@Override
	public void actionPerformed(ActionEvent evt) {
		Object src = evt.getSource();
		if (src == menu) {

			Game.getInstance().setupMenu();
		}
	}
}
