package tank.game.map;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import tank.game.Game;
import tank.game.entity.Entity;
import tank.game.entity.PlayerType;
import tank.game.entity.Tank;
import tank.game.entity.TileEntity;
import tank.game.entity.powerup.Powerup;
import tank.game.entity.powerup.PowerupType;
import tank.game.gfx.SpriteSheet;
import tank.game.screen.Screen;

public class GameHandler {

	public static boolean debug = false;

	private Tank player1Tank;
	private Tank player2Tank;

	private Level level;

	private ArrayList<Entity> entities = new ArrayList<Entity>();

	private Screen screen = Game.getInstance().getScreen();

	public String winner = null;
	private long checkPowerupIn;

	public void init() {

		level = new Level(Screen.WIDTH, Screen.HEIGHT);

		try {
			level.loadLevel();
		}
		catch (IOException ex) {
			ex.printStackTrace();
		}

		for (int x = 0; x < level.tiles.length; x++) {
			for (int y = 0; y < level.tiles[x].length; y++) {
				if (level.tiles[x][y] != null) {
					TileEntity te = new TileEntity(x * SpriteSheet.SPRITE_SIZE, y + SpriteSheet.SPRITE_SIZE, level.tiles[x][y]);
					te.spawnIn();
				}
			}
		}

		player1Tank = new Tank(50, 100, PlayerType.ONE);
		player1Tank.spawnIn();

		player2Tank = new Tank(500, 100, PlayerType.TWO);
		player2Tank.spawnIn();

		screen.addKeyListener(player1Tank);
		screen.addKeyListener(player2Tank);

	}

	public void addEntity(Entity entity) {
		entities.add(entity);
	}

	public void removeEntity(Entity entity) {
		entity.destroy = true;

	}

	public ArrayList<Entity> getEntities() {
		ArrayList<Entity> oldEntities = new ArrayList<Entity>();
		oldEntities.addAll(entities);

		return oldEntities;
	}

	public void tick() {
		if (System.currentTimeMillis() > checkPowerupIn) {
			if (new Random().nextInt(100) < 5) {
				Powerup p = new Powerup(new Random().nextInt(Screen.WIDTH - 20), new Random().nextInt(Screen.HEIGHT - 20 - (Screen.HEALTH_BAR_HEIGHT * 2)), PowerupType.values()[new Random().nextInt(PowerupType.values().length)], 20);

				p.spawnIn();
				checkPowerupIn = System.currentTimeMillis() + 5000;
			}
		}
		ArrayList<Entity> oldEntities = new ArrayList<Entity>();
		/**
		 * Create "clone" of entities list so that we can freely remove and
		 * iterate through the list without concurrency issues.
		 */
		oldEntities.addAll(entities);

		for (Entity e : oldEntities) {

			/**
			 * If an entity is tagged for destruction remove them from the
			 * actual list.
			 */
			if (e.destroy) {
				entities.remove(e);
				continue;
			}

			e.tick();

			for (Entity other : oldEntities) {
				if (other.destroy) {
					continue;
				}
				if (e != other) {
					if (e.collides(other)) {
						e.collision(other);
						other.collision(e);
					}
				}
			}

		}

	}

	public Tank getPlayer1Tank() {
		return player1Tank;
	}

	public Tank getPlayer2Tank() {
		return player2Tank;
	}

	public Level getLevel() {
		return level;
	}

	public Screen getScreen() {
		return screen;
	}

}
