package tank.game.gfx;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.awt.image.FilteredImageSource;
import java.awt.image.ImageFilter;
import java.awt.image.ImageProducer;
import java.awt.image.RGBImageFilter;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

import tank.game.render.Texture;

public class SpriteSheet {

	public static final int SPRITE_SIZE = 63;
	public static final int BORDER_WIDTH = 1;

	public static final int TANK_NORMAL = 0;
	public static final int TANK_FLASH_RED = 1;
	public static final int TANK_FLASH_WHITE = 2;
	public static final int MISSILE = 3;
	public static final int OSAMA = 30;
	public static final int BOMB = 31;

	public static final int ENEMY_TANK = 11;

	public static final int EXPLOSION_1 = 20;
	public static final int EXPLOSION_2 = 21;
	public static final int EXPLOSION_3 = 22;
	public static final int EXPLOSION_4 = 23;
	public static final int EXPLOSION_5 = 24;
	public static final int EXPLOSION_6 = 25;
	public static final int EXPLOSION_7 = 26;
	public static final int CACTUS = 41;
	public static final int TREE = 43;
	public static final int ROCK = 44;

	public String path;
	public int width;
	public int height;

	private Texture[] images = new Texture[100];

	public SpriteSheet(String path) {
		BufferedImage image = null;
		// reading the sprite
		try {
			image = ImageIO.read(SpriteSheet.class.getResourceAsStream(path));
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		// if image is null (just in case)
		if (image == null) {
			return;
		}
		this.path = path;
		this.width = image.getWidth();
		this.height = image.getHeight();

		int count = 0;
		for (int i = BORDER_WIDTH; i < width; i += (SPRITE_SIZE + BORDER_WIDTH)) {
			for (int j = BORDER_WIDTH; j < height; j += (SPRITE_SIZE + BORDER_WIDTH)) {

				BufferedImage bi = image.getSubimage(j, i, SPRITE_SIZE, SPRITE_SIZE);

				bi = makeColorTransparent(bi);

				int subHeight = 0, subWidth = 0;

				int tempH = 0, tempW = 0;

				for (int x = 0; x < bi.getWidth(); x++) {
					for (int y = 0; y < bi.getHeight(); y++) {
						if (new Color(bi.getRGB(x, y), true).getAlpha() != 0) {
							tempH++;
						}
					}

					if (tempH > subHeight) {
						subHeight = tempH;
					}
					tempH = 0;

				}

				for (int x = 0; x < bi.getHeight(); x++) {
					for (int y = 0; y < bi.getWidth(); y++) {
						if (new Color(bi.getRGB(y, x), true).getAlpha() != 0) {
							tempW++;
						}

					}
					if (tempW > subWidth) {
						subWidth = tempW;
					}
					tempW = 0;

				}

				images[count] = new Texture(bi, subWidth, subHeight);

				count++;
			}
		}

	}

	public static BufferedImage makeColorTransparent(final BufferedImage im) {
		final ImageFilter filter = new RGBImageFilter() {

			public final int filterRGB(final int x, final int y, final int rgb) {
				Color c = new Color(rgb);

				if (c.getRed() > 190 && c.getGreen() < 30 && c.getBlue() > 190) {
					return 0x00FFFFFF & rgb;
				} else {
					return rgb;
				}
			}
		};

		final ImageProducer ip = new FilteredImageSource(im.getSource(), filter);
		Image image = Toolkit.getDefaultToolkit().createImage(ip);

		final BufferedImage bufferedImage = new BufferedImage(image.getWidth(null), image.getHeight(null), BufferedImage.TYPE_INT_ARGB);
		final Graphics2D g2 = bufferedImage.createGraphics();

		g2.drawImage(image, 0, 0, null);
		g2.dispose();

		return bufferedImage;
	}

	public Texture getSprite(int index) {
		return images[index];
	}

}
