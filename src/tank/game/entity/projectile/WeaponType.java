package tank.game.entity.projectile;

import java.util.Random;

import tank.game.gfx.SpriteSheet;

public enum WeaponType {
	TANK_SHELL(9, SpriteSheet.MISSILE, 15, 500, "tank_shoot.wav");

	private WeaponType(double damage, int texture, int speed, int cooldown, String sound) {
		this.damage = damage;
		this.texture = texture;
		this.speed = speed;
		this.cooldown = cooldown;
		this.sound = sound;
	}

	private double damage;
	private int texture;
	private int speed;
	private int cooldown;
	private String sound;

	public int getCooldown() {
		return cooldown;
	}

	public String getSound() {
		return sound;
	}

	public double getDamage() {
		return damage;
	}

	public int getTexture() {
		return texture;
	}

	public int getSpeed() {
		return speed;
	}

}
