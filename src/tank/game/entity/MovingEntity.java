package tank.game.entity;

import java.awt.Rectangle;

import tank.game.Game;
import tank.game.player.Orientation;
import tank.game.screen.Screen;

public abstract class MovingEntity extends Entity {

	protected boolean moving;

	public MovingEntity(int x, int y) {
		super(x, y);
	}

	@Override
	public void tick() {
		if (moving) {
			int yWould = y;
			int xWould = x;

			yWould += (orientation.getY() * getSpeed());
			xWould += (orientation.getX() * getSpeed());

			if (xWould < 0 || xWould >= Screen.WIDTH - 30 || yWould < 0 || yWould > Screen.HEIGHT - 30) {
				if (!isSolid()) {
					destroy();
				}
				return;
			}

			if (isSolid()) {

				/**
				 * Iterate through all other entities and check for collision
				 * that will prevent movement
				 */
				for (Entity entity : Game.getInstance().getGameHandler().getEntities()) {
					if (entity != this && entity.isSolid()) {
						Rectangle entRect = entity.getBoundingBox();
						Rectangle wouldbe = getBoundingBox();

						Orientation othertoThis = null;

						int xa = entRect.x - wouldbe.x;
						int ya = entRect.y - wouldbe.y;

						/**
						 * These lines of code prevent a tank from getting right
						 * next to another tank, and switching directions,
						 * therefore causing them to be stuck in that position,
						 * since when the orientation changes the rectangles may
						 * have been forced to intersect.
						 */
						if (Math.abs(xa) > Math.abs(ya)) {
							if (xa > 0) {
								othertoThis = Orientation.EAST;
							} else {
								othertoThis = Orientation.WEST;
							}
						} else {

							if (ya > 0) {
								othertoThis = Orientation.SOUTH;
							} else {
								othertoThis = Orientation.NORTH;
							}

						}

						if (othertoThis.getOpposite() != orientation) {
							if (entRect.intersects(wouldbe)) {
								return;
							}
						}
					}
				}
			}

			x = xWould;
			y = yWould;

		}
	}

	@Override
	public boolean collision(Entity other) {
		return false;

	}

	public abstract int getSpeed();

}
