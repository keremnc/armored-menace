package tank.game.entity.projectile;

import tank.game.Game;
import tank.game.entity.Entity;
import tank.game.entity.Tank;
import tank.game.entity.powerup.PowerupType;
import tank.game.render.Texture;

public class Missile extends Projectile {
	private Tank shooter;
	WeaponType mType;

	public Missile(int x, int y, Tank shooter, WeaponType mType) {
		super(x, y);
		moving = true;
		this.shooter = shooter;
		this.mType = mType;

	}

	@Override
	public Texture getTexture() {
		return Game.getInstance().getSpriteSheet().getSprite(mType.getTexture());
	}

	@Override
	public int getSpeed() {
		return mType.getSpeed();
	}

	@Override
	public boolean collision(Entity other) {
		if (other instanceof Tank) {
			if (other == shooter) {
				return false;
			}
			((Tank) other).hurt(this);
		}

		if (other.isSolid()) {
			Game.getInstance().getGameHandler().removeEntity(this);
		}

		return true;
	}

	public WeaponType getType() {
		return mType;
	}

	public Tank getShooter() {
		return shooter;
	}

	public double getDamage() {
		return mType.getDamage() * (shooter.hasPowerup(PowerupType.EXPLOSIVITY) ? 2 : 1);
	}

}
