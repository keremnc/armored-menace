package tank.game.menu;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import tank.game.Game;

public class MainMenu extends JFrame implements ActionListener {

	public static final int WIDTH = 345;
	public static final int HEIGHT = (WIDTH / 13 * 4);
	public static final int SCALE = 3;

	private Button singlePlayer, quit, about;
	private JPanel iconPanel = new JPanel();

	public MainMenu() {

		setMinimumSize(new Dimension(WIDTH * SCALE, HEIGHT * SCALE));
		setMaximumSize(new Dimension(WIDTH * SCALE, HEIGHT * SCALE));
		setPreferredSize(new Dimension(WIDTH * SCALE, HEIGHT * SCALE));
		setupButtons();
	}

	private void setupButtons() {
		setLocationRelativeTo(null);

		singlePlayer = new Button("Start Game");
		singlePlayer.setPreferredSize(new Dimension(120, 40));

		about = new Button("About");

		quit = new Button("Quit");

		iconPanel.setLayout(new BoxLayout(iconPanel, BoxLayout.Y_AXIS));

		GridLayout gl = new GridLayout(3, 3, 100, 15);
		iconPanel.setLayout(gl);

		iconPanel.add(singlePlayer);
		iconPanel.add(about);
		iconPanel.add(quit);

		JPanel mini = new JPanel();

		mini.add(iconPanel, BorderLayout.NORTH);
		mini.setBackground(Game.BG_COLOR);

		iconPanel.setBackground(Game.BG_COLOR);
		setBackground(Game.BG_COLOR);

		add(mini, BorderLayout.SOUTH);

		singlePlayer.addActionListener(this);
		about.addActionListener(this);
		quit.addActionListener(this);

		mini.setVisible(true);

	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		g.drawImage(Game.getInstance().getImageHandler().getLogo(), 0, 0, Game.BG_COLOR, this);

	}

	@Override
	public void actionPerformed(ActionEvent evt) {
		Object src = evt.getSource();
		if (src == singlePlayer) {
			Game.getInstance().startGame();
		} else if (src == about) {
			Game.getInstance().showAbout();
		} else if (src == quit) {
			Game.getInstance().stop();
		}
	}
}
