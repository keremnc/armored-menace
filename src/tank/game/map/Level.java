package tank.game.map;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Random;

import tank.game.gfx.SpriteSheet;

public class Level {

	public Tile[][] tiles;
	private Location topLeft = new Location(0, 0);

	public Level(int mapWidth, int mapHeight) {
		tiles = new Tile[(int) Math.ceil(mapWidth / SpriteSheet.SPRITE_SIZE)][(int) Math.ceil(mapHeight / SpriteSheet.SPRITE_SIZE)];
	}

	public Tile[][] getTiles() {
		return tiles;
	}

	public Location getTopLeft() {
		return topLeft;
	}

	public void loadLevel() throws IOException {
		InputStream is = Level.class.getResourceAsStream("/maps/desert.map");

		InputStreamReader isr = new InputStreamReader(is);
		BufferedReader br = new BufferedReader(isr);
		String line;
		StringBuilder sb = new StringBuilder();

		while ((line = br.readLine()) != null) {

			sb.append(line);
			line = br.readLine();

		}
		String everything = sb.toString();

		fromData(everything);

		br.close();
		isr.close();
		is.close();

	}

	public void fromData(String data) {

		for (String str : data.split(",")) {
			String name = str.split("-")[0];
			int x = Integer.parseInt(str.split("-")[1]);
			int y = Integer.parseInt(str.split("-")[2]);

			tiles[x][y] = Tile.valueOf(name);
		}

	}
}
