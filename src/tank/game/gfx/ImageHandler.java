package tank.game.gfx;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

public class ImageHandler {

	private SpriteSheet sheet;
	private BufferedImage logo;

	public ImageHandler(SpriteSheet sheet) {
		this.sheet = sheet;
		loadImages();
	}

	private void loadImages() {
		try {
			logo = ImageIO.read(ImageHandler.class.getResourceAsStream("/images/logo.png"));

		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	public BufferedImage getLogo() {
		return logo;
	}
}
