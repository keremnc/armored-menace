package tank.game.entity;

import java.awt.Rectangle;

import tank.game.Game;
import tank.game.entity.projectile.Missile;
import tank.game.entity.projectile.WeaponType;
import tank.game.player.Orientation;
import tank.game.render.Texture;

public abstract class Entity {
	protected long spawnTime;
	public int x, y;
	public Orientation orientation = Orientation.NORTH;
	public boolean destroy;
	public boolean dead;

	public Entity(int x, int y) {
		this.x = x;
		this.y = y;
	}

	/**
	 * Draws the entity to the screen.
	 */
	public void render() {
		if (Game.getInstance().getScreen() != null) {
			Game.getInstance().getScreen().render(getTexture(), orientation, x, y, getBoundingBox());
		}
	}

	/**
	 * Method called every tick. Entity subclasses must override this method and
	 * provide some sort of behavior, or none at all.
	 */
	public abstract void tick();

	/**
	 * Gets the texture that will be drawn on the screen.
	 */
	public abstract Texture getTexture();

	/**
	 * Spawns the entity, by adding it to the list of alive entities
	 */
	public void spawnIn() {
		spawnTime = System.currentTimeMillis();
		Game.getInstance().getGameHandler().addEntity(this);
	}

	/**
	 * Gets whether or not this entity can be passed through
	 */
	public boolean isSolid() {
		return true;
	}

	/**
	 * Flags this entity for removal and the next iteration of the
	 */
	public void destroy() {
		Game.getInstance().getGameHandler().removeEntity(this);
	}

	/**
	 * Gets whether or not an entity collides with this entity
	 */
	public boolean collides(Entity other) {
		Rectangle r = getBoundingBox();
		// checks if both of their rectangles intersect
		return r.intersects(other.getBoundingBox());
	}

	public abstract boolean collision(Entity other);

	/**
	 * Gets the visual representation and most accurate representation of the
	 * bounding box for the entity object
	 */
	public Rectangle getBoundingBox() {

		int centX = (x + x + 63) / 2;
		int centY = (y + y + 63) / 2;

		int width = orientation == Orientation.EAST || orientation == Orientation.WEST ? getTexture().getWidth() : getTexture().getHeight();
		int height = orientation == Orientation.EAST || orientation == Orientation.WEST ? getTexture().getHeight() : getTexture().getWidth();

		centX -= width / 2;
		centY -= height / 2;

		return new Rectangle(centX, centY, width, height);
	}
}
