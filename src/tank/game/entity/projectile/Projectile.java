package tank.game.entity.projectile;

import tank.game.entity.MovingEntity;

public abstract class Projectile extends MovingEntity {

	public Projectile(int x, int y) {
		super(x, y);
	}

	@Override
	public boolean isSolid() {
		return false;
	}

}
